user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

timer_resolution 100ms;
worker_priority  10;

events {
    worker_connections  2048;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    client_header_buffer_size   128k;
    large_client_header_buffers 100 256k;

    gzip                        on;
    gzip_comp_level             2;
    gzip_vary                   on;
    gzip_min_length             1;
    gzip_proxied                any;
    gzip_types                  text/plain 
                                text/css 
                                application/x-javascript 
                                text/xml 
                                application/xml 
                                application/xml+rss 
                                text/javascript 
                                application/javascript 
                                application/json;
    gzip_buffers                32 1024k;
    server_names_hash_max_size  2048;
    server_names_hash_bucket_size 512;
    postpone_output             1460;

    sendfile        on;
    tcp_nopush      on;
    tcp_nodelay     on;

    keepalive_timeout 75 20;
    charset           utf-8;

    client_header_timeout   10;
    client_body_timeout     10;
    output_buffers          8 512k;
    lingering_time          30;
    lingering_timeout       6;

    include /etc/nginx/conf.d/*.conf;
}
