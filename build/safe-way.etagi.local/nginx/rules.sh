#!/bin/bash
echo Оптимизируем права;
find "/var/hosting/safe-way.etagi.com/" -type d -exec chmod 0755 '{}' \;
find "/var/hosting/safe-way.etagi.com/" -type f -exec chmod 0644 '{}' \;
chown -R nginx:nginx /var/hosting/safe-way.etagi.com/;
chmod -R 0777 /var/hosting/safe-way.etagi.com/backup;
chown -R postgres:postgres /var/hosting/safe-way.etagi.com/backup;
echo Установка прав OK;
